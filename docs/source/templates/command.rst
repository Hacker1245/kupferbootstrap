.. title: {{fullname}}

.. click:: {% if fullname == 'main' %}main:cli{% else %}{{fullname}}.cli:cmd_{{fullname}}{% endif %}
  :prog: kupferbootstrap {{fullname}}
  :nested: full

