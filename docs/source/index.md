# Kupferbootstrap Documentation

This is the documentation for [Kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap),
a tool to build and flash packages and images for the [Kupfer](https://gitlab.com/kupfer/) mobile Linux distro.

## Documentation pages

```{toctree}
usage/index
cli
```
